<?php

namespace Traffic\Cars;

use Traffic\Roads\Road;
use Traffic\TrafficLights\TrafficLight;

class Car
{
    private $position;
    private $speed;

    private $road;

    public function __construct(Road $road, int $position, int $speed = 1)
    {
        $this->road = $road;
        if ($position > $this->road->length) {
            throw new \Exception('The car with position ' . $position . ' is out of road.');
        }
        $this->position = $position;
        $this->speed = $speed;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;
        return $this;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function setSpeed(int $speed): self
    {
        $this->speed = $speed;
        return $this;
    }

    public function getRoad(): ?Road
    {
        return $this->road;
    }

    public function setRoad(?Road $road): self
    {
        $this->road = $road;
        return $this;
    }

    public function moveOrStay()
    {
        if (!is_null($this->position)) {
            if (
                $this->road
                && in_array($this->position, $this->road->busyPositions)
                && $this->road->trafficLight->getActiveColor() == TrafficLight::COLOR_GREEN
            ) {
                $this->road->removeBusyPosition();
            }

            $firstBusyPosition = end($this->road->busyPositions);
            if ($firstBusyPosition) {
                $lastFreePosition = $firstBusyPosition - 1;
            } else {
                $lastFreePosition = $this->road->getLength();
            }

            if (
                $this->position < $lastFreePosition
                || $this->road->trafficLight->getActiveColor() == TrafficLight::COLOR_GREEN
            ) {
                $roadLength = $this->road->getLength();
                if ($this->position == $this->road->getLength()) {
                    $this->setPosition(null);
                    $this->setRoad(null);
                } else {
                    if ($this->position + $this->speed < $roadLength) {
                        $this->position += $this->speed;
                    } else {
                        $this->position = $roadLength;
                    }
                }
            }

            if (
                $this->position == $lastFreePosition
                && in_array($this->road->trafficLight->getActiveColor(), [
                    TrafficLight::COLOR_RED,
                    TrafficLight::COLOR_YELLOW
                ])
            ) {
                $this->road->addBusyPosition();
            }

        }
    }
}
