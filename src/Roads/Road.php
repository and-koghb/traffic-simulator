<?php

namespace Traffic\Roads;

use Traffic\TrafficLights\TrafficLight;

class Road
{
    public $length;
    public $trafficLight;

    public $busyPositions = [];

    public function getLength(): int
    {
        return $this->length;
    }

    public function setLength(int $length): self
    {
        $this->length = $length;
        return $this;
    }

    public function getTrafficLight(): ?TrafficLight
    {
        return $this->trafficLight;
    }

    public function setTrafficLight(?TrafficLight $trafficLight): self
    {
        $this->trafficLight = $trafficLight;
        return $this;
    }

    public function addBusyPosition(): self
    {
        $position = end($this->busyPositions);
        if (!$position) {
            $position = $this->length;
        }
        if (in_array($position, $this->busyPositions)) {
            $position --;
        }
        while (!in_array($position, $this->busyPositions) && $position >= 0) {
            $this->busyPositions[] = $position;
        }
        return $this;
    }

    public function removeBusyPosition(): self
    {
        if ($this->busyPositions) {
            array_pop($this->busyPositions);
        }
        return $this;
    }
}
