<?php

namespace Traffic\TrafficLights;

abstract class TrafficLight
{
    const COLOR_RED = 'red';
    const COLOR_YELLOW = 'yellow';
    const COLOR_GREEN = 'green';

    const RED_TIME = 0;
    const YELLOW_TIME = 0;
    const GREEN_TIME = 0;

    protected $activeColor;
    protected $currentSecond;

    public function getActiveColor(): string
    {
        return $this->activeColor;
    }

    public function setActiveColor(string $activeColor): self
    {
        $this->activeColor = $activeColor;
        return $this;
    }

    public function getCurrenSecond(): int
    {
        return $this->currentSecond;
    }

    public function setCurrenSecond(int $currentSecond): self
    {
        $this->currentSecond = $currentSecond;
        return $this;
    }

    public function getAvailavleColorsWithTimes(): array
    {
        $ct = $this->getAllColorsWithTimes();
        foreach ($ct as $color => $time) {
            if ($time == 0) {
                unset($ct[$color]);
            }
        }
        return $ct;
    }

    protected function getAllColorsWithTimes(): array
    {
        return [
            self::COLOR_RED => static::RED_TIME,
            self::COLOR_GREEN => static::GREEN_TIME,
            self::COLOR_YELLOW => static::YELLOW_TIME,
        ];
    }

    public function tick(): self
    {
        if ($this->currentSecond > 1) {
            $this->currentSecond --;
        } else {
            $ct = $this->getAvailavleColorsWithTimes();
            $colors = array_keys($ct);
            if ($this->activeColor == end($colors)) {
                $this->activeColor = $colors[0];
            } else {
                $activeColorKey = array_search($this->activeColor, $colors);
                $this->activeColor = $colors[$activeColorKey + 1];
            }
            $this->currentSecond = $ct[$this->activeColor];
        }
        return $this;
    }

    public function setRandomState(): self
    {
        $ct = $this->getAvailavleColorsWithTimes();
        $color = array_rand($ct);
        $second = rand(1, $ct[$color]);
        $this->setActiveColor($color);
        $this->setCurrenSecond($second);
        return $this;
    }
}
