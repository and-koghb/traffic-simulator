<?php

namespace Traffic\TrafficLights;

class OurTrafficLight extends TrafficLight
{
    const RED_TIME = 10;
    const YELLOW_TIME = 0;
    const GREEN_TIME = 10;
}
