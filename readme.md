## Original task

    Consider a 4-lane traffic junction, controlled by one set of traffic
    lights which are located at position 50. When a vehicle reaches the
    same position as the traffic lights, it must obey the state of the
    lights. If the lights are red, the vehicle cannot move forward. If
    the lights are green, the vehicle can move forward. The traffic
    lights change every 10 seconds.
    
    Assumptions
    
    All traffic is moving in the same direction.
    When traveling, all vehicles move at the same, constant speed, of one
    vehicle length per second.
    Initial vehicle positions are 0, 12, 20, 24 The simulation runs until
    all vehicles pass through the traffic lights.
    
    Spend no more than 1 to 2 hours creating a solution to display this
    simulation. Your solution should:
    
    - be written using PHP
    - function as expected without any defects
    - show good OO design principles (coupling and encapsulation)
    - represent the type of code that you would produce if we hired you on a long term contract
    
### It's possible to change car speed.