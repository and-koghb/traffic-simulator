<?php

require_once('vendor/autoload.php');

use Traffic\Cars\Car;
use Traffic\Roads\Road;
use Traffic\TrafficLights\OurTrafficLight;

$road = new Road;
$road->setLength(50);

$trafficLight = new OurTrafficLight;
$trafficLight->setRandomState();

$road->setTrafficLight($trafficLight);

try {
    $car1 = new Car($road, 0);
    $car2 = new Car($road, 12);
    $car3 = new Car($road, 20);
    $car4 = new Car($road, 24);

    $allPassed = 0;
    while ($allPassed <= 1) {
        echo $trafficLight->getActiveColor() . ' --- ' . $trafficLight->getCurrenSecond() . '<br>';
        echo 'car1 --- ' . $car1->getPosition() . '<br>';
        echo 'car2 --- ' . $car2->getPosition() . '<br>';
        echo 'car3 --- ' . $car3->getPosition() . '<br>';
        echo 'car4 --- ' . $car4->getPosition() . '<br>';
        echo '------------------------------<br>';

        $road->trafficLight->tick();
        $car1->moveOrStay();
        $car2->moveOrStay();
        $car3->moveOrStay();
        $car4->moveOrStay();

        if (
            is_null($car1->getPosition())
            && is_null($car2->getPosition())
            && is_null($car3->getPosition())
            && is_null($car4->getPosition())
        ) {
            $allPassed ++;
        }
    }
} catch (\Exception $e) {
    echo $e->getMessage();
}
